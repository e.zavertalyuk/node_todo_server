import projects from '../mock/projects.js';
import { MongoClient, ServerApiVersion } from 'mongodb';
import { priceList } from '../mock/prices.js';
const uri = "mongodb+srv://ezavertalyuk:VSQdM7n7EmM78UeU@cluster0.fzpzupj.mongodb.net/?retryWrites=true&w=majority";

const client = new MongoClient(uri, {
   serverApi: {
      version: ServerApiVersion.v1,
      strict: true,
      deprecationErrors: true,
   }
});

async function run() {
   try {
      await client.connect();
      const db = await client.db("Projects");
      const projectCollection = await db.collection("ProjectsCollection");
      const prod = await projectCollection.insertMany(projects)
   } finally {
      await client.close()
   }
}
// run();

export async function registerUser(userData) {
   try {
      await client.connect();
      const db = await client.db('Users');
      const usersCollection = await db.collection('UsersList');
      const { insertedId } = await usersCollection.insertOne(userData)
      const newUser = await usersCollection.findOne({ _id: insertedId });
      return newUser

   } finally {
      await client.close()
   }
}

export async function loginUser(credentials) {
   try {
      await client.connect();
      const db = await client.db('Users');
      const usersCollection = await db.collection('UsersList');

      const user = await usersCollection.findOne({
         username: credentials.username,
         password: credentials.password
      });

      if (!user) {
         throw new Error('Authentication failed. Please register.');
      }

      return user;

   } finally {
      await client.close();
   }
}

export async function getPrices() {
   try {
      await client.connect();
      const db = await client.db('PriceList');
      const priceCollection = await db.collection('PriceCollection');
      const allPrices = await priceCollection.find({}).toArray()
      return allPrices
   } finally {
      await client.close()
   }
}
// async function insertPrices() {
//    try {
//       await client.connect();
//       const db = await client.db('PriceList');
//       const priceCollection = await db.collection('PriceCollection');
//       await priceCollection.insertMany(priceList);
//    } finally {
//       await client.close();
//    }
// }
// insertPrices()

export async function getProjects() {
   try {
      await client.connect();
      const db = await client.db('Projects');
      const prodCollection = await db.collection('ProjectsCollection');
      const allProjects = await prodCollection.find({}).toArray()
      return allProjects
   } finally {
      await client.close()
   }
}

export async function getComments() {
   try {
      await client.connect();
      const db = await client.db('MassageComments');
      const commentsCollection = await db.collection('CommentsCollection');
      const allComments = await commentsCollection.find({}).toArray()
      return allComments
   } finally {
      await client.close()
   }
}

export async function addComment(comentBody) {
   try {
      await client.connect();
      const db = await client.db('MassageComments');
      const commentsCollection = await db.collection('CommentsCollection');
      const { insertedId } = await commentsCollection.insertOne(comentBody)
      const newComment = await commentsCollection.findOne({ _id: insertedId });
      return newComment

   } finally {
      await client.close()
   }
}


export async function getTodos() {
   try {
      await client.connect();
      const db = await client.db('todosList');
      const todoCollection = await db.collection('todoList');
      const allTodos = await todoCollection.find({}).toArray()
      return allTodos
   } finally {
      await client.close()
   }
}



export async function getTodoById(id) {
   try {
      await client.connect();
      const db = await client.db('todosList');
      const todoCollection = await db.collection('todoList');
      const todo = await todoCollection.findOne({ id: parseInt(id) });
      return todo;
   } catch (error) {
      console.log('Error retrieving todo:', error);
      throw error;
   } finally {
      await client.close();
   }
}


export async function addTodo(todo) {
   try {
      await client.connect();
      const db = await client.db('todosList');
      const todoCollection = await db.collection('todoList');
      const { insertedId } = await todoCollection.insertOne(todo)
      const newTodo = await todoCollection.findOne({ _id: insertedId });
      return newTodo

   } finally {
      await client.close()
   }
}

export async function toggleTodo(todoId) {
   try {
      await client.connect();
      const db = await client.db('todosList');
      const todoCollection = await db.collection('todoList');
      const todo = await todoCollection.findOne({ id: parseInt(todoId) });
      if (!todo) {
         console.log('Todo not found');
         return;
      }
      const newCompleted = !todo.completed;
      const result = await todoCollection.updateOne(
         { id: parseInt(todoId) },
         { $set: { completed: newCompleted } }
      );
      if (result.modifiedCount === 1) {
         console.log('Todo complete status updated successfully');
      } else {
         console.log('Failed to update todo');
      }
   } finally {
      await client.close();
   }
}

export async function deleteTodo(todoId) {
   try {
      await client.connect();
      const db = await client.db('todosList');
      const todoCollection = await db.collection('todoList');
      const deleteResult = await todoCollection.findOneAndDelete({ id: parseInt(todoId) });
      if (deleteResult.deletedCount === 1) {
         return { success: true };
      } else {
         return { success: false, message: 'Todo not found' };
      }
   } finally {
      await client.close()
   }
}


