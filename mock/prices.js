export const priceList = [
  {
    name: "Лімфодренажний",
    duration: 60,
    price: 550,
  },
  {
    name: "Лімфодренажний",
    duration: 90,
    price: 800,
  },
  {
    name: "Загальний класичний",
    duration: 60,
    price: 600,
  },
  {
    name: "Загальний класичний",
    duration: 90,
    price: 850,
  },
  {
    name: "Загальний класичний",
    duration: 120,
    price: 1100,
  },
  {
    name: "Шийно-комірцева зона",
    duration: 30,
    price: 450,
  },
  {
    name: "Шийно-комірцева зона",
    duration: 60,
    price: 600,
  },
  {
    name: "Спортивний",
    duration: 60,
    price: 600,
  }, {
    name: "Спортивний",
    duration: 90,
    price: 850,
  },
  {
    name: "Спортивний",
    duration: 120,
    price: 1100,
  },
  {
    name: "Релакс масаж",
    duration: 60,
    price: 600,
  }, {
    name: "Релакс масаж",
    duration: 90,
    price: 850,
  },
  {
    name: "Релакс масаж",
    duration: 120,
    price: 1100,
  },
  {
    name: "Антицелюлітний",
    duration: 60,
    price: 650,
  }, {
    name: "Антицелюлітний",
    duration: 90,
    price: 900,
  },
  {
    name: "Антицелюлітний",
    duration: 120,
    price: 1200,
  },
  {
    name: "Масаж ніг",
    duration: 30,
    price: 350,
  }, {
    name: "Масаж ніг",
    duration: 60,
    price: 500,
  },
  {
    name: "Дитячий масаж",
    duration: 60,
    price: 650,
  },

];
