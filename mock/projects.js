const projects = [
   {
      id: 1,
      name: "Innovation Oasis",
      link: "https://final-project-mauve-seven.vercel.app/",
      description: "A fully functioning electronics store. Technologies employed: React, Redux, react-router-dom, as well asmultiple APIs for currency conversion, Nova Poshta delivery, andothers.",
      image: "img1",
      gitHub: "https://github.com/PoladMamedov/final_project",
      stack: {
         first: "",
         second: "",
         third: ""
      }
   },
   {
      id: 2,
      name: "Doctors App",
      link: "https://egorzave.github.io/doctor_app/",
      description: "Fully responsive Web application for making appointmens to doctor Used technologies: ES6 Classes, AJAX (fetch), ES6 modules, SCSS, BEM,Bootstrap, Gulp.",
      info: "Login: testCv@gmail.com ; Password: 123456789",
      image: "img1",
      gitHub: "https://github.com/EgorZave/doctor_app",
      stack: {
         first: "",
         second: "",
         third: ""
      }
   },
   {
      id: 3,
      name: "Hum Landing",
      link: "https://egorzave.github.io/HumLanding/",
      description: "Personal Project Utilizing HTML, CSS, and JavaScript. The landing page features a modern and clean design with smooth animationsand transitions..",
      image: "img1",
      gitHub: "https://github.com/EgorZave/HumLanding",
      stack: {
         first: "",
         second: "",
         third: ""
      }
   },
   {
      id: 4,
      name: "Forkio",
      link: "https://egorzave.github.io/Forkio_Step/",
      description: "Forkio has been performed in team of 2 persons. The Stack of technologies applied are Preprocessor SCSS, BEM, Adaptive design, Task manager GULP, GRIDand Flexbox, Vanila JS, NPM, VCS GitLab, Deployment GitHub Pages.",
      image: "img1",
      gitHub: "https://github.com/EgorZave/Forkio_Step",
      stack: {
         first: "",
         second: "",
         third: ""
      }

   },
   {
      id: 5,
      name: "To-do List + BackEnd",
      link: "https://node-todo-front.vercel.app/",
      description: "The application interacts with the server and allows you to add/delete/edit the to-do list. The server was written by me",
      image: "img1",
      gitHub: "https://gitlab.com/e.zavertalyuk/node_todo_front",
      stack: {
         first: "",
         second: "",
         third: ""
      }
   },
   {
      id: 6,
      name: "Laptops Store",
      link: "https://notebooksstore.vercel.app/",
      description: "Online laptop store with the ability to purchase / add to favorites a specific product.",
      image: "img1",
      gitHub: "https://gitlab.com/e.zavertalyuk/notebookspage",
      stack: {
         first: "",
         second: "",
         third: ""
      }
   },
   {
      id: 7,
      name: "Weather App",
      link: "https://weather-app-jade-xi.vercel.app/",
      description: "The application allows you to view the weather in the selected city",
      image: "img1",
      gitHub: "https://gitlab.com/e.zavertalyuk/weather_app",
      stack: {
         first: "",
         second: "",
         third: ""
      }
   },

]
export default projects
