import express from 'express'
import cors from 'cors'
import { addTodo, getTodos, getTodoById, toggleTodo, getProjects, registerUser, loginUser, getPrices, getComments, addComment } from "./db/index.js";
import { deleteTodo } from "./db/index.js";

const app = express();
app.use(cors());
app.use(express.json());


app.get('/', function (req, res) {
   res.send('hello from backEnd');
});

app.post('/signin', async function (req, res) {
   const newUser = await registerUser(req.body)
   res.send(newUser)
});

app.post('/login', async function (req, res) {
   try {
      const user = await loginUser(req.body);
      res.send(user);
   } catch (error) {
      res.status(401).send({ error: error.message });
   }
});

app.post('/todo', async function (req, res) {
   const newTodo = await addTodo(req.body)
   res.send(newTodo)
});

app.get('/todos', async function (req, res) {
   const todos = await getTodos();
   res.send(todos)
});

app.get('/prices', async function (req, res) {
   const priceList = await getPrices();
   res.send(priceList)
});
app.get('/comments', async function (req, res) {
   const commentsList = await getComments();
   res.send(commentsList)
});
app.post('/comment', async function (req, res) {
   const newComment = await addComment(req.body)
   res.send(newComment)
});


app.get('/projects', async function (req, res) {
   const projects = await getProjects();
   res.send(projects)
});


app.patch('/todos/:id', async function (req, res) {
   const id = req.params.id;

   try {
      await toggleTodo(id);
      res.send('Success');
   } catch (error) {
      console.log(error);
      res.status(500).send('Error completed todo');
   }
})

app.get('/todos/:id', async function (req, res) {
   const id = req.params.id;
   try {
      const todo = await getTodoById(id);
      console.log(todo);
      if (todo) {
         res.send(todo);
      } else {
         res.status(404).send('Todo not found');
      }
   } catch (error) {
      console.log(error);
      res.status(500).send('Error retrieving todo');
   }
});
app.delete('/todos/:id', async function (req, res) {
   const id = req.params.id;
   try {
      await deleteTodo(id);
      res.send('Success');
   } catch (error) {
      console.log(error);
      res.status(500).send('Error deleting todo');
   }
});

app.listen(3001, function () {
   console.log('Server is running on port 3001');
});
